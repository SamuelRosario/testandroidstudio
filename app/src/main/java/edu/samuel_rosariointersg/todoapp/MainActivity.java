package edu.samuel_rosariointersg.todoapp;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        // Inflate your view
        setContentView(R.layout.activity_main);

        // Get references to UI widgets
        final ListView myListView = (ListView)findViewById(R.id.myListView);
        final EditText myEditText = (EditText)findViewById(R.id.myEditText);
        final Button addButton = (Button) findViewById(R.id.addbutton);

        // Create the ArrayList and the ArrayAdapter
        final ArrayList<String> todoItems = new ArrayList<String>();
        final ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,todoItems);

        // Bind the listview to the array adapter
        myListView.setAdapter(aa);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                todoItems.add(0, myEditText.getText().toString());
                myEditText.setText("");
                aa.notifyDataSetChanged();
            }
        });

        myListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, final View view, int i, long l) {
                int itemPosition     = i;

                // ListView Clicked item value
                String  itemValue    = (String) myListView.getItemAtPosition(i);

                // Show Alert
                Toast.makeText(getApplicationContext(), "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG).show();
                final String item = (String) adapterView.getItemAtPosition(i);
                view.animate().setDuration(2000).alpha(0)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                todoItems.remove(item);
                                aa.notifyDataSetChanged();
                                view.setAlpha(1);
                            }
                        });
            } // ListView Clicked item index

        });

        myListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, final View view, int i, long l) {
                int itemPosition     = i;

                // ListView Clicked item value
                String  itemValue    = (String) myListView.getItemAtPosition(i);

                // Show Alert
                Toast.makeText(getApplicationContext(), "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG).show();
                final String item = (String) adapterView.getItemAtPosition(i);
                view.animate().setDuration(2000).alpha(0)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                todoItems.remove(item);
                                aa.notifyDataSetChanged();
                                view.setAlpha(1);
                            }
                        });
                return false;
            }
        });


    }

}